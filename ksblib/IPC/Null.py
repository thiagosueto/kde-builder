import type_enforced
from typing import override

from .IPC import IPC


@type_enforced.Enforcer
class IPC_Null(IPC):
    """
    Dummy IPC module in case SysVIPC doesn't work or async mode is not needed.
    """
    
    def __init__(self):
        IPC.__init__(self)
        self.msgList = []  # List of messages.
    
    @override
    def sendMessage(self, msg) -> bool:
        self.msgList.append(msg)
        return True
    
    @override
    def receiveMessage(self) -> bytes | None:
        if not len(self.msgList) > 0:
            return None
        return self.msgList.pop(0)
