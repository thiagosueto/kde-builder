import os
import re
import subprocess
import textwrap
import time
import type_enforced
from typing import NoReturn

from .OSSupport import OSSupport
from .BuildException import BuildException
from .BuildContext import BuildContext
from .Debug import Debug


@type_enforced.Enforcer
class FirstRun:
    """
    DESCRIPTION
    
    Performs initial-install setup, implementing the ``--initial-setup`` option.
    
    SYNOPSIS
    ::
    
        exitcode = ksblib.FirstRun.setupUserSystem()
        exit(exitcode)
    """
    
    BASE_SHELL_SNIPPET = textwrap.dedent("""\
    # kdesrc-build #################################################################
    
    ## Add kdesrc-build to PATH
    """)
    
    SHELL_SEPARATOR_SNIPPET = textwrap.dedent("""\
    ################################################################################
    """)
    
    def __init__(self):
        self.oss = OSSupport()
        self.baseDir = None
    
    @staticmethod
    def yesNoPrompt(msg: str) -> bool:
        answ = input(f"{msg} (y/N) ")
        return answ == "y"
    
    def setupUserSystem(self, baseDir, setup_steps: list) -> NoReturn:
        self.baseDir = baseDir
        envShell = os.getenv("SHELL", "undefined")
        shellName = envShell.split("/")[-1]
        
        try:
            if "install-distro-packages" in setup_steps:
                print(Debug().colorize("=== install-distro-packages ==="))
                self._installSystemPackages()
            if "generate-config" in setup_steps:
                print(Debug().colorize("=== generate-config ==="))
                self._setupBaseConfiguration()
            if "update-shellrc" in setup_steps:
                print(Debug().colorize("=== update-shellrc ==="))
                self._setupShellRcFile(shellName)
        except BuildException as e:
            msg = e.message
            print(Debug().colorize(f"  b[r[*] r[{msg}]"))
            exit(1)
        
        exit(0)
    
    # Internal functions
    
    @staticmethod
    def _readPackages(vendor, version) -> dict:
        """
        Reads from the files from data/pkg and dumps the contents in a hash keyed by filename (the "[pkg/vendor/version]" part between each resource)
        """
        packages = {}
        cur_key = ""
        with open(os.path.dirname(os.path.realpath(__file__)) + f"/../data/pkg/{vendor}.ini", "r") as file:
            while line := file.readline():
                line = line.removesuffix("\n")
                if match := re.match(r"^\[ *([^ ]+) *]$", line):
                    cur_key = match.group(1)
                    packages[cur_key] = []
                    continue
                if line.startswith("#") or line == "":
                    continue
                packages[cur_key].append(line)
        return packages
    
    @staticmethod
    def _throw(msg: str) -> NoReturn:
        raise BuildException.make_exception("Setup", msg)
    
    def _installSystemPackages(self) -> None:
        
        vendor = self.oss.vendorID()
        osVersion = self.oss.vendorVersion()
        
        print(Debug().colorize(f" b[-] Installing b[system packages] for b[{vendor}]..."))
        
        packages = self._findBestVendorPackageList()
        if not packages:
            print(Debug().colorize(f" r[b[*] Packages could not be installed, because kdesrc-build does not know your distribution ({vendor})"))
            return
        
        installCmd = self._findBestInstallCmd()
        cmd = " ".join(installCmd + packages)
        
        # Remake the command for Arch Linux to not require running sudo command when not needed (https://bugs.kde.org/show_bug.cgi?id=471542)
        if self.oss.vendorID() == "arch":
            required_packages_and_required_groups = packages
            missing_packages_and_required_groups = subprocess.run("pacman -T " + " ".join(required_packages_and_required_groups), shell=True, capture_output=True, check=False).stdout.decode("utf-8").removesuffix("\n").split("\n")
            all_possible_groups = subprocess.run("pacman -Sg", shell=True, capture_output=True, check=False).stdout.decode("utf-8").removesuffix("\n").split("\n")
            required_groups = [el for el in missing_packages_and_required_groups if el in all_possible_groups]
            missing_packages_not_grouped = [el for el in missing_packages_and_required_groups if el not in required_groups]
            missing_packages_from_required_groups = []
            if required_groups:
                for required_group in required_groups:
                    missing_packages_from_required_group = subprocess.run(f"pacman -Sqg {required_group} | xargs pacman -T", shell=True, capture_output=True, check=False).stdout.decode("utf-8").removesuffix("\n").split("\n")
                    missing_packages_from_required_group = list(filter(None, missing_packages_from_required_group))  # Remove empty string element. It appears if there is no any unresolved package from the group
                    missing_packages_from_required_groups += missing_packages_from_required_group
            packages = missing_packages_not_grouped + missing_packages_from_required_groups
            if not packages:
                cmd = "# All dependencies are already installed. No need to run pacman. :)"
            else:
                cmd = " ".join(installCmd + packages)
        
        print(Debug().colorize(f" b[*] Running 'b[{cmd}]'"))
        
        result = subprocess.run(cmd, shell=True, check=True)
        exitStatus = result.returncode
        
        # Install one at a time if we can, but check if sudo is present
        hasSudo = subprocess.call("type " + "sudo", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE) == 0
        everFailed = False
        if exitStatus != 0 and self.oss.isDebianBased() and hasSudo:
            for onePackage in packages:
                commandLine = f"sudo apt-get -q -y --no-install-recommends install {onePackage}"
                print(Debug().colorize(f" b[*] Running 'b[{commandLine}]'"))
                # Allow for Ctrl+C.
                time.sleep(250 / 1000)
                result = subprocess.run(cmd, shell=True, capture_output=True, check=True)
                if not everFailed:
                    everFailed = result.returncode != 0
        
        exitStatus = 0  # It is normal if some packages are not available.
        if everFailed:
            print(Debug().colorize(" y[b[*] Some packages failed to install, continuing to build."))
        
        if exitStatus == 0:
            print(Debug().colorize(" b[*] b[g[Looks like the necessary packages were successfully installed!]"))
        else:
            print(Debug().colorize(f" r[b[*] Failed with exit status {exitStatus}. Ran into an error with the installer!"))
    
    def suggestedNumCoresForLowMemory(self) -> int:
        """
        Returns the suggested number of cores to use for make jobs for build jobs where
        memory is a bottleneck, such as qtwebengine.
        ::
        
            num_cores = ksb.FirstRun.suggestedNumCoresForLowMemory()
        """
        
        # Try to detect the amount of total memory for a corresponding option for
        # heavyweight modules
        
        # 4 GiB is assumed if no info on memory is available, as this will
        # calculate to 2 cores.
        mem_total = self.oss.detectTotalMemory()
        if not mem_total:
            mem_total = 4 * 1024 * 1024
        
        rounded_mem = int(mem_total / 1024000.0)
        return max(1, int(rounded_mem / 2))  # Assume 2 GiB per core
    
    # Return the highest number of cores we can use based on available memory, but
    # without exceeding the base number of cores available.
    def _getNumCoresForLowMemory(self, num_cores) -> int:
        return min(self.suggestedNumCoresForLowMemory(), num_cores)
    
    def _setupBaseConfiguration(self) -> None:
        # According to XDG spec, if $XDG_CONFIG_HOME is not set, then we should
        # default to ~/.config
        xdgConfigHome = os.environ.get("XDG_CONFIG_HOME", os.environ.get("HOME") + "/.config")
        xdgConfigHomeShort = xdgConfigHome.replace(os.environ.get("HOME"), "~")  # Replace $HOME with ~
        knownLocations = [os.getcwd() + "/kdesrc-buildrc",
                          f"{xdgConfigHome}/kdesrc-buildrc",
                          os.environ.get("HOME") + "/.kdesrc-buildrc"]
        locatedFile = None
        for knownLocation in knownLocations:
            if os.path.isfile(knownLocation):
                locatedFile = knownLocation
                break
        
        if locatedFile:
            printableLocatedFile = locatedFile.replace(os.environ.get("HOME"), "~")
            print(Debug().colorize(f"b[*] You already have a configuration file: b[y[{printableLocatedFile}]"))
            return
        
        print(Debug().colorize(f"b[*] Creating b[sample configuration file]: b[y[\"{xdgConfigHomeShort}/kdesrc-buildrc\"]..."))
        
        with open(os.path.dirname(os.path.realpath(__file__)) + "/../data/kdesrc-buildrc.in", "r") as data_file:
            sampleRc = data_file.read()
        
        numCores = None
        if self.oss.vendorID() == "linux":
            numCores = int(subprocess.run(["nproc"], shell=True, capture_output=True, check=True).stdout.decode("utf-8").removesuffix("\n"))
        elif self.oss.vendorID() == "freebsd":
            numCores = int(subprocess.run(["sysctl", "-n", "hw.ncpu"], shell=True, capture_output=True, check=True).stdout.decode("utf-8").removesuffix("\n"))
        
        if not numCores:
            numCores = 4
        
        numCoresLow = self._getNumCoresForLowMemory(numCores)
        build_include_dir = re.sub(r"^" + os.environ["HOME"], "~", self.baseDir) + "/data/build-include"
        
        sampleRc = sampleRc.replace("%{num_cores}", str(numCores))
        sampleRc = sampleRc.replace("%{num_cores_low}", str(numCoresLow))
        sampleRc = sampleRc.replace("%{build_include_dir}", build_include_dir)
        
        gl = BuildContext().build_options["global"]  # real global defaults
        
        def fill_placeholder(option_name, mode=""):
            value = gl[option_name]
            if mode == "bool_to_str":
                # Perl doesn't have native boolean types, so config internally operates on 0 and 1.
                # But it will be convenient to users to use "true"/"false" strings in their config files.
                value = "true" if value else "false"
            elif mode == "home_to_tilde":
                value = re.sub(rf"^{os.environ.get("HOME")}", "~", value)
            nonlocal sampleRc
            sampleRc = sampleRc.replace(f"%{{{option_name}}}", value)
        
        fill_placeholder("include-dependencies", "bool_to_str")
        fill_placeholder("install-dir", "home_to_tilde")
        fill_placeholder("source-dir", "home_to_tilde")
        fill_placeholder("build-dir", "home_to_tilde")
        fill_placeholder("install-session-driver", "bool_to_str")
        fill_placeholder("install-environment-driver", "bool_to_str")
        fill_placeholder("stop-on-failure", "bool_to_str")
        fill_placeholder("directory-layout")
        fill_placeholder("compile-commands-linking", "bool_to_str")
        fill_placeholder("compile-commands-export", "bool_to_str")
        fill_placeholder("generate-vscode-project-config", "bool_to_str")
        
        with open(f"{xdgConfigHome}/kdesrc-buildrc", "w") as sampleFh:
            sampleFh.write(sampleRc)
        print()
    
    def _setupShellRcFile(self, shellName) -> None:
        rcFilepath = None
        printableRcFilepath = None
        extendedShell = 1
        
        if shellName == "bash":
            rcFilepath = os.environ.get("HOME") + "/.bashrc"
        elif shellName == "zsh":
            if "ZDOTDIR" in os.environ:
                rcFilepath = os.environ["ZDOTDIR"] + "/.zshrc"
            else:
                rcFilepath = os.environ.get("HOME") + "/.zshrc"
        elif shellName == "fish":
            if "XDG_CONFIG_HOME" in os.environ:
                rcFilepath = os.environ.get("XDG_CONFIG_HOME") + "/fish/conf.d/kdesrc-build.fish"
            else:
                rcFilepath = os.environ.get("HOME") + ".config/fish/conf.d/kdesrc-build.fish"
        else:
            rcFilepath = os.environ.get("HOME") + "/.profile"
            print(Debug().colorize(f" y[b[*] Couldn't detect the shell, using {rcFilepath}."))
            extendedShell = 0
        
        with open(f"{self.baseDir}/data/kdesrc-run-completions.sh", "r") as file:
            kdesrc_run_completions = file.read()
        
        # Used for bash/zsh and requires non-POSIX syntax support.
        EXT_SHELL_RC_SNIPPET = kdesrc_run_completions + FirstRun.SHELL_SEPARATOR_SNIPPET
        
        printableRcFilepath = rcFilepath.replace(os.environ.get("HOME"), "~")
        addToShell = self.yesNoPrompt(Debug().colorize(f" b[*] Update your b[y[{printableRcFilepath}]?"))
        
        if addToShell:
            with open(rcFilepath, "a") as rcFh:
                rcFh.write("")
                if shellName != "fish":
                    rcFh.write(self.BASE_SHELL_SNIPPET + f"export PATH=\"{self.baseDir}:$PATH\"\n")
                    if extendedShell:
                        rcFh.write(EXT_SHELL_RC_SNIPPET)
                else:
                    rcFh.write(self.BASE_SHELL_SNIPPET + f"fish_add_path --global {self.baseDir}\n")
            
            print(Debug().colorize(textwrap.dedent("""\
            
                 - Added b[y[kde-builder] directory into PATH
                 - Added b[y[kdesrc-builder-launch] shell function
             b[*] b[g[Shell rc-file is successfully setup].""")))
        
        else:
            print(Debug().colorize(textwrap.dedent("""\
            
             b[*] You can manually configure your shell rc-file with the snippet below:""")))
            print(self.BASE_SHELL_SNIPPET + f"export PATH=\"{self.baseDir}:$PATH\"\n")
            if extendedShell:
                print(EXT_SHELL_RC_SNIPPET)
    
    def _findBestInstallCmd(self) -> list:
        cmdsRef = {
            "cmd/install/alpine/unknown": "apk add --virtual .makedeps-kdesrc-build",
            "cmd/install/arch/unknown": "pacman -S --noconfirm",
            "cmd/install/debian/unknown": "apt-get -q -y --no-install-recommends install",
            "cmd/install/fedora/unknown": "dnf -y install",
            "cmd/install/freebsd/unknown": "pkg install -y",
            "cmd/install/gentoo/unknown": "emerge -v --noreplace",
            "cmd/install/opensuse/unknown": "zypper install -y --no-recommends",
        }
        
        supportedDistros = [cmddist.removeprefix("cmd/install/").removesuffix("/unknown") for cmddist in cmdsRef.keys()]
        
        bestVendor = self.oss.bestDistroMatch(supportedDistros)
        print(Debug().colorize(f"    Using installer for b[{bestVendor}]"))
        
        version = self.oss.vendorVersion()
        cmd = []
        
        for opt in [f"{bestVendor}/{version}", f"{bestVendor}/unknown"]:
            key = f"cmd/install/{opt}"
            if key in cmdsRef.keys():
                cmd = cmdsRef[key].split(" ")
                break
        
        if not cmd:
            self._throw(f"No installer for {bestVendor}!")
        
        # If not running as root already, add sudo
        if os.geteuid() != 0:
            cmd.insert(0, "sudo")
        
        return cmd
    
    def _findBestVendorPackageList(self) -> list:
        # Debian handles Ubuntu also
        supportedDistros = ["alpine", "arch", "debian", "fedora", "freebsd", "gentoo", "mageia", "opensuse"]
        bestVendor = self.oss.bestDistroMatch(supportedDistros)
        version = self.oss.vendorVersion()
        print(Debug().colorize(f"    Installing packages for b[{bestVendor}]/b[{version}]"))
        return self._packagesForVendor(bestVendor, version)
    
    def _packagesForVendor(self, vendor, version) -> list:
        packages = self._readPackages(vendor, version)
        for opt in [f"pkg/{vendor}/{version}", f"pkg/{vendor}/unknown"]:
            if opt in packages.keys():
                return packages[opt]
        return []
