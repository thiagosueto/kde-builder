import type_enforced
from typing import override, NoReturn

from .ModuleSet import ModuleSet
from ..BuildException import BuildException


@type_enforced.Enforcer
class ModuleSet_Null(ModuleSet):
    """
    Used automatically by <Module> to represent the absence of a <ModuleSet> without
    requiring definedness checks.
    """
    
    def __init__(self):
        # No need to call parent __init__.
        pass
        self.name = ""  # pl2py: support returning name by not invoking the function. Needed for `wayland` module for example.
    
    @override
    def name(self) -> str:
        return ""
    
    @staticmethod
    @override
    def convertToModules(*_) -> NoReturn:
        BuildException.croak_internal("kdesrc-build should not have made it to this call. :-(")
