# Test sorting modules into build order

import unittest
# use Test::More;
# use POSIX;
# use File::Basename;

from ksblib.DependencyResolver import DependencyResolver

# # <editor-fold desc="Begin collapsible section">
# my $timestamp1 = POSIX::strftime("%s", localtime);
# my $filename = basename(__FILE__);
# my $section_header = "File: $filename (click to toggle collapse)";
# print "\e[0Ksection_start:${timestamp1}:$filename\[collapsed=true]\r\e[0K$section_header\n";  # displayed in collapsible section in gitlab ci job log
# # </editor-fold>


class TestApp(unittest.TestCase):
    def setUp(self):
        self.graph1 = {
            "a": {
                "votes": {
                    "b": 1,
                    "d": 1
                },
                "build": 1,
                "module": {
                    "name": "a",
                    "#create-id": 1,
                },
            },
            "b": {
                "votes": {},
                "build": 1,
                "module": {
                    "name": "b",
                    "#create-id": 1,
                },
            },
            "c": {
                "votes": {
                    "d": 1
                },
                "build": 1,
                "module": {
                    "name": "c",
                    "#create-id": 1,
                },
            },
            "d": {
                "votes": {},
                "build": 1,
                "module": {
                    "name": "d",
                    "#create-id": 1,
                },
            },
            "e": {
                "votes": {},
                "build": 1,
                "module": {
                    "name": "e",
                    "#create-id": 2, # Should come after everything else
                },
            },
        }
        
    def test1(self):
        expected1 = [self.graph1[item]["module"] for item in ["a", "c", "b", "d", "e"]]
        actual1 = DependencyResolver.sortModulesIntoBuildOrder(self.graph1)
        
        self.assertLessEqual(actual1, expected1, "should sort modules into the proper build order")
    
    def test2(self):
        # use some random key strokes for names:
        # unlikely to yield keys in equivalent order as $graph1: key order *should not matter*
        graph2 = {
            "avdnrvrl": self.graph1["c"],
            "lexical1": self.graph1["b"],
            "lexicla3": self.graph1["e"],
            "nllfmvrb": self.graph1["a"],
            "lexical2": self.graph1["d"],
        }
        
        # corresponds to same order as the test above
        expected2 = [graph2[item]["module"] for item in ["nllfmvrb", "avdnrvrl", "lexical1", "lexical2", "lexicla3"]]
        actual2 = DependencyResolver.sortModulesIntoBuildOrder(graph2)
        
        self.assertListEqual(actual2, expected2, "key order should not matter for build order")
    
    def test3(self):
        graph3 = {
            "a": self.graph1["a"],
            "b": self.graph1["b"],
            "c": self.graph1["c"],
            "d": self.graph1["d"],
            "e": self.graph1["e"],
        }
        graph3["a"]["build"] = 0
        graph3["b"]["module"] = None  # Empty module blocks should be treated as build == 0
        
        expected3 = [graph3[item]["module"] for item in ("c", "d", "e")]
        actual3 = DependencyResolver.sortModulesIntoBuildOrder(graph3)
        
        self.assertListEqual(actual3, expected3, "modules that are not to be built should be omitted")

# # <editor-fold desc="End collapsible section">
# my $timestamp2 = POSIX::strftime("%s", localtime);
# print "\e[0Ksection_end:${timestamp2}:$filename\r\e[0K\n";  # close collapsible section
# # </editor-fold>
