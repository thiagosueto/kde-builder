# Test detection of dependency cycles in a dependency graph

import unittest
# use Test::More;
# use POSIX;
# use File::Basename;

from ksblib.DependencyResolver import DependencyResolver

# # <editor-fold desc="Begin collapsible section">
# my $timestamp1 = POSIX::strftime("%s", localtime);
# my $filename = basename(__FILE__);
# my $section_header = "File: $filename (click to toggle collapse)";
# print "\e[0Ksection_start:${timestamp1}:$filename\[collapsed=true]\r\e[0K$section_header\n";  # displayed in collapsible section in gitlab ci job log
# # </editor-fold>


class TestApp(unittest.TestCase):
    def test1(self):
        # trivial cycle a -> a
        graph1 = {
            "a": {
                "deps": {
                    "a": {}
                }
            },
            "b": {
                "deps": {}
            }
        }
        
        self.assertTrue(DependencyResolver()._detectDependencyCycle(graph1, "a", "a"), "should detect 'trivial' cycles of an item to itself")

    def test2(self):
        graph2 = {
            "a": {
                "deps": {
                    "b": {}
                }
            },
            "b": {
                "deps": {
                    "a": {}
                }
            }
        }
        
        self.assertTrue(DependencyResolver()._detectDependencyCycle(graph2, "a", "a"), "should detect cycle: a -> b -> a")
        self.assertTrue(DependencyResolver()._detectDependencyCycle(graph2, "b", "b"), "should detect cycle: b -> a -> b")

    def test3(self):
        # no cycles, should therefore not 'detect' any false positives
        graph3 = {
            "a": {
                "deps": {
                    "b": {}
                }
            },
            "b": {
                "deps": {}
            }
        }
        
        self.assertFalse(DependencyResolver()._detectDependencyCycle(graph3, "a", "a"), "should not report false positives for 'a'")
        self.assertFalse(DependencyResolver()._detectDependencyCycle(graph3, "b", "b"), "should not report false positives for 'b'")

# # <editor-fold desc="End collapsible section">
# my $timestamp2 = POSIX::strftime("%s", localtime);
# print "\e[0Ksection_end:${timestamp2}:$filename\r\e[0K\n";  # close collapsible section
# # </editor-fold>
