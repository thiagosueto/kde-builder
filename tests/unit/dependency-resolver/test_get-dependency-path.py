# Verify that _getDependencyPathOf() works properly

import unittest
# use Test::More;
# use POSIX;
# use File::Basename;

from ksblib.DependencyResolver import DependencyResolver
from ksblib.Module.Module import Module

# # <editor-fold desc="Begin collapsible section">
# my $timestamp1 = POSIX::strftime("%s", localtime);
# my $filename = basename(__FILE__);
# my $section_header = "File: $filename (click to toggle collapse)";
# print "\e[0Ksection_start:${timestamp1}:$filename\[collapsed=true]\r\e[0K$section_header\n";  # displayed in collapsible section in gitlab ci job log
# # </editor-fold>


# Redefine ksb::Module to stub fullProjectPath() results
class Module_Test(Module):
    def __init__(self, projectPath, kde):
        self.projectPath = projectPath
        self.kde = kde
        # super().__init__(projectPath, kde)  # todo needed?
    
    def isKDEProject(self):
        return self.kde
    
    def fullProjectPath(self):
        return self.projectPath


class TestApp(unittest.TestCase):
    def test1(self):
        module1 = Module_Test("test/path", 1)
        
        self.assertEqual(DependencyResolver()._getDependencyPathOf(module1, "foo", "bar"), "test/path", "should return full project path if a KDE module object is passed")
        
        module2 = None
        
        self.assertEqual(DependencyResolver()._getDependencyPathOf(module2, "foo", "bar"), "bar", "should return the provided default if no module is passed")
        
        module3 = Module_Test("test/path", 0)
        self.assertEqual(DependencyResolver()._getDependencyPathOf(module3, "foo", "bar"), "third-party/test/path", "should return 'third-party/' prefixed project path if a non-KDE module object is passed")

# # <editor-fold desc="End collapsible section">
# my $timestamp2 = POSIX::strftime("%s", localtime);
# print "\e[0Ksection_end:${timestamp2}:$filename\r\e[0K\n";  # close collapsible section
# # </editor-fold>
