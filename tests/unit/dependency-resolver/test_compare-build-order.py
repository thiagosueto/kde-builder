# Test comparison operation for sorting modules into build order

import unittest
# use ksb;
# use Test::More;
# use POSIX;
# use File::Basename;

from ksblib.DependencyResolver import DependencyResolver

# # <editor-fold desc="Begin collapsible section">
# my $timestamp1 = POSIX::strftime("%s", localtime);
# my $filename = basename(__FILE__);
# my $section_header = "File: $filename (click to toggle collapse)";
# print "\e[0Ksection_start:${timestamp1}:$filename\[collapsed=true]\r\e[0K$section_header\n";  # displayed in collapsible section in gitlab ci job log
# # </editor-fold>


class TestApp(unittest.TestCase):
    def test1(self):
        graph1 = {
            "a": {
                "votes": {
                    "b": 1,
                    "d": 1
                },
                "module": {
                    "name": "a",
                },
            },
            "b": {
                "votes": {},
                "module": {
                    "name": "b",
                },
            },
            "c": {
                "votes": {
                    "d": 1
                },
                "module": {
                    "name": "c",
                },
            },
            "d": {
                "votes": {},
                "module": {
                    "name": "d",
                },
            },
        
            "e": {  # Here to test sorting by rc-file order
                "votes": {
                    "b": 1,
                    "d": 1,
                },
                "module": {
                    "name": "e",
                    "#create-id": 2,
                },
            },
            "f": {  # Identical to 'e' except it's simulated earlier in rc-file
                "votes": {
                    "b": 1,
                    "d": 1,
                },
                "module": {
                    "name": "f",
                    "#create-id": 1,
                },
            },
        }
        
        # Test that tests are symmetric e.g. a > b => b < a. This permits us to only manually
        # test one pair of these tests now that the test matrix is growing.
        for l in ["a", "b", "c", "d", "e", "f"]:
            for r in ["a", "b", "c", "d", "e", "f"]:
                res = DependencyResolver()._compareBuildOrder(graph1, l, r)
                
                if l == r:
                    self.assertEqual(res, 0, f"'{l}' should be sorted at the same position as itself")
                else:
                    self.assertEqual(abs(res), 1, f"Different module items ('{l}' and '{r}') compare to 1 or -1 (but not 0)")
                    self.assertEqual(DependencyResolver()._compareBuildOrder(graph1, r, l), -res, f"Swapping order of operands should negate the result ('{r}' vs '{l}')")
        
        self.assertEqual(DependencyResolver()._compareBuildOrder(graph1, "a", "b"), -1, "'a' should be sorted before 'b' by dependency ordering")
        self.assertEqual(DependencyResolver()._compareBuildOrder(graph1, "a", "c"), -1, "'a' should be sorted before 'c' by vote ordering")
        self.assertEqual(DependencyResolver()._compareBuildOrder(graph1, "a", "d"), -1, "'a' should be sorted before 'd' by dependency ordering")
        self.assertEqual(DependencyResolver()._compareBuildOrder(graph1, "a", "e"), -1, "'a' should be sorted before 'e' by lexicographic ordering")
        self.assertEqual(DependencyResolver()._compareBuildOrder(graph1, "a", "f"), -1, "'a' should be sorted before 'f' by lexicographic ordering")
        
        self.assertEqual(DependencyResolver()._compareBuildOrder(graph1, "b", "c"),  1, "'b' should be sorted after 'c' by vote ordering")
        self.assertEqual(DependencyResolver()._compareBuildOrder(graph1, "b", "d"), -1, "'b' should be sorted before 'd' by lexicographic ordering")
        self.assertEqual(DependencyResolver()._compareBuildOrder(graph1, "b", "e"),  1, "'b' should be sorted after 'e' by dependency ordering")
        self.assertEqual(DependencyResolver()._compareBuildOrder(graph1, "b", "f"),  1, "'b' should be sorted after 'f' by dependency ordering")
        
        self.assertEqual(DependencyResolver()._compareBuildOrder(graph1, "c", "d"), -1, "'c' should be sorted before 'd' by dependency ordering")
        self.assertEqual(DependencyResolver()._compareBuildOrder(graph1, "c", "e"),  1, "'c' should be sorted after 'e' by vote ordering")
        self.assertEqual(DependencyResolver()._compareBuildOrder(graph1, "c", "f"),  1, "'c' should be sorted after 'f' by vote ordering")
        
        self.assertEqual(DependencyResolver()._compareBuildOrder(graph1, "d", "e"),  1, "'d' should be sorted after 'e' by dependency ordering")
        self.assertEqual(DependencyResolver()._compareBuildOrder(graph1, "d", "f"),  1, "'d' should be sorted after 'f' by dependency ordering")
        
        self.assertEqual(DependencyResolver()._compareBuildOrder(graph1, "e", "f"),  1, "'e' should be sorted after 'f' by rc-file ordering")

# # <editor-fold desc="End collapsible section">
# my $timestamp2 = POSIX::strftime("%s", localtime);
# print "\e[0Ksection_end:${timestamp2}:$filename\r\e[0K\n";  # close collapsible section
# # </editor-fold>
