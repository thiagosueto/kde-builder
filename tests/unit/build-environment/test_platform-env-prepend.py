# Test that empty install-dir and/or qt-install-dir do not cause empty /bin settings to be
# configured in environment.

import unittest
import os
import re
# use ksb;
# use Test::More;
# use POSIX;
# use File::Basename;
#
# use ksb::DependencyResolver;
from ksblib.BuildContext import BuildContext
from ksblib.Module.Module import Module

# # <editor-fold desc="Begin collapsible section">
# my $timestamp1 = POSIX::strftime("%s", localtime);
# my $filename = basename(__FILE__);
# my $section_header = "File: $filename (click to toggle collapse)";
# print "\e[0Ksection_start:${timestamp1}:$filename\[collapsed=true]\r\e[0K$section_header\n";  # displayed in collapsible section in gitlab ci job log
# # </editor-fold>


class TestApp(unittest.TestCase):
    def test1(self):
        ctx = BuildContext()
        
        def no_bare_bin(arg):
            elem = arg.split(":")
            return not any(x == "/bin" for x in elem)
        
        mod = Module(ctx, "test")
        newPath = os.environ.get("PATH")
        newPath = re.sub(r"^/bin:", "", newPath)  # Remove existing bare /bin entries if present
        newPath = re.sub(r":/bin$", "", newPath)
        newPath = re.sub(r":/bin:", "", newPath)
        os.environ["PATH"] = newPath
        
        ctx.setOption({"install-dir": ""})  # must be set but empty
        ctx.setOption({"qt-install-dir": "/dev/null"})
    
        mod.setupEnvironment()
    
        self.assertTrue("PATH" in ctx.env, "Entry created for PATH when setting up mod env")
        self.assertTrue(no_bare_bin(ctx.env["PATH"]), "/bin wasn't prepended to PATH")
        
        # --
        
        ctx.resetEnvironment()
        
        mod = Module(ctx, "test")
        newPath = os.environ.get("PATH")
        newPath = re.sub(r"^/bin:", "", newPath)  # Remove existing bare /bin entries if present
        newPath = re.sub(r":/bin$", "", newPath)
        newPath = re.sub(r":/bin:", "", newPath)
        os.environ["PATH"] = newPath
        
        ctx.setOption({"qt-install-dir": ""})  # must be set but empty
        ctx.setOption({"install-dir": "/dev/null"})
        
        mod.setupEnvironment()
        
        self.assertTrue("PATH" in ctx.env, "Entry created for PATH when setting up mod env")
        self.assertTrue(no_bare_bin(ctx.env["PATH"]), "/bin wasn't prepended to PATH")
        
        # --
        
        ctx.resetEnvironment()
        
        mod = Module(ctx, "test")
        newPath = os.environ.get("PATH")
        newPath = re.sub(r"^/bin:", "", newPath)  # Remove existing bare /bin entries if present
        newPath = re.sub(r":/bin$", "", newPath)
        newPath = re.sub(r":/bin:", "", newPath)
        os.environ["PATH"] = newPath
        
        ctx.setOption({"qt-install-dir": "/dev/null"})
        ctx.setOption({"install-dir": "/dev/null"})
        
        mod.setupEnvironment()
        
        self.assertTrue("PATH" in ctx.env, "Entry created for PATH when setting up mod env")
        self.assertTrue(no_bare_bin(ctx.env["PATH"]), "/bin wasn't prepended to PATH")
        
        # --
        
        # Ensure binpath and libpath options work
        
        ctx.resetEnvironment()
        
        mod = Module(ctx, "test")
        os.environ["PATH"] = "/bin:/usr/bin"
        
        ctx.setOption({"binpath": "/tmp/fake/bin"})
        ctx.setOption({"libpath": "/tmp/fake/lib:/tmp/fake/lib64"})
        
        mod.setupEnvironment()
        
        self.assertTrue(re.match(r"/tmp/fake/bin", ctx.env["PATH"]), "Ensure `binpath` present in generated PATH")
        self.assertTrue(re.match(ctx.env["PATH"], ctx.env["LD_LIBRARY_PATH"]), "Ensure `libpath` present in generated LD_LIBRARY_PATH")

# # <editor-fold desc="End collapsible section">
# my $timestamp2 = POSIX::strftime("%s", localtime);
# print "\e[0Ksection_end:${timestamp2}:$filename\r\e[0K\n";  # close collapsible section
# # </editor-fold>
