# Test prune_under_directory_p, including ability to remove read-only files in
# sub-tree

import unittest
import os
import tempfile
from ksblib.Util.Util import Util
from ksblib.BuildContext import BuildContext
# use Mojo::File qw(path);
from promise import Promise
from pathlib import Path

# use Test::More;
# use File::Temp;
# use POSIX;
# use File::Basename;

# # <editor-fold desc="Begin collapsible section">
# my $timestamp1 = POSIX::strftime("%s", localtime);
# my $filename = basename(__FILE__);
# my $section_header = "File: $filename (click to toggle collapse)";
# print "\e[0Ksection_start:${timestamp1}:$filename\[collapsed=true]\r\e[0K$section_header\n";  # displayed in collapsible section in gitlab ci job log
# # </editor-fold>


class TestApp(unittest.TestCase):
    def test1(self):
        dir = tempfile.mkdtemp(prefix="kdesrc-build-testXXXXXX")
        self.assertTrue(dir, "tempdir created")
        
        file = os.path.join(dir, "a")
        open(file, "a").close()
        self.assertTrue(os.path.exists(file), "first file created")
        
        new_permissions = 0o444
        os.chmod(file, new_permissions)
        self.assertTrue(os.stat(file).st_mode & new_permissions == new_permissions, "Changed mode to readonly")
        
        ctx = BuildContext()
        ctx.setOption({"log-dir": os.path.abspath(dir)})
        promise = Util.prune_under_directory_p(ctx, os.path.abspath(dir))
        
        # This shouldn't disappear until we let the promise start!
        # self.assertTrue(os.path.exists(file), "prune_under_directory_p does not start until we let promise run")  # pl2py: we use promise that starts as we make it
        
        Promise.wait(promise)
        
        self.assertFalse(os.path.exists(file), "Known read-only file removed")
        
        files = list(Path(dir).rglob("*"))
        if len(files) == 0:
            self.assertEqual(len(files), 0, f"entire directory {dir} removed")
        else:
            files = [str(file) for file in files]
            self.assertEqual(len(files), 0, "Files in temp dir: " + ", ".join(files))

# # <editor-fold desc="End collapsible section">
# my $timestamp2 = POSIX::strftime("%s", localtime);
# print "\e[0Ksection_end:${timestamp2}:$filename\r\e[0K\n";  # close collapsible section
# # </editor-fold>
