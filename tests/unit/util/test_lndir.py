# Test safe_lndir_p

import os.path
import unittest
from ksblib.Util.Util import Util
import tempfile
from promise import Promise

# use Test::More;
# use File::Temp;
# use POSIX;
# use File::Basename;

# # <editor-fold desc="Begin collapsible section">
# my $timestamp1 = POSIX::strftime("%s", localtime);
# my $filename = basename(__FILE__);
# my $section_header = "File: $filename (click to toggle collapse)";
# print "\e[0Ksection_start:${timestamp1}:$filename\[collapsed=true]\r\e[0K$section_header\n";  # displayed in collapsible section in gitlab ci job log
# # </editor-fold>


class TestApp(unittest.TestCase):
    def test1(self):
        dir = tempfile.mkdtemp(prefix="kdesrc-build-testXXXXXX")
        self.assertTrue(dir, "tempdir created")
        
        file = os.path.join(dir, "a")
        open(file, "a").close()
        self.assertTrue(os.path.exists(file), "first file created")
        
        dir2 = os.path.join(dir, "b/c")
        os.makedirs(dir2)
        self.assertTrue(os.path.isdir(f"{dir}/b/c"), "dir created")
        
        file2 = os.path.join(dir, "b", "c", "file2")
        open(file2, "a").close()
        self.assertTrue(os.path.exists(f"{dir}/b/c/file2"), "second file created")
        
        to = tempfile.mkdtemp(prefix="kdesrc-build-test2")
        promise = Util.safe_lndir_p(os.path.abspath(dir), os.path.abspath(to))
        
        # These shouldn't exist until we let the promise start!
        # self.assertFalse(os.path.exists(f"{to}/b/c/file2"), "safe_lndir does not start until we let promise run")  # pl2py: the promises we use start right after we create them
        
        Promise.wait(promise)
        
        self.assertTrue(os.path.isdir(f"{to}/b/c"), 'directory symlinked over')
        self.assertTrue(os.path.islink(f"{to}/a"), 'file under directory is a symlink')
        self.assertTrue(os.path.exists(f"{to}/a"), "file under directory exists")
        self.assertFalse(os.path.exists(f"{to}/b/d/file3"), "nonexistent file does not exist")
        self.assertTrue(os.path.islink(f"{to}/b/c/file2"), 'file2 under directory is a symlink')
        self.assertTrue(os.path.exists(f"{to}/b/c/file2"), "file2 under directory exists")

# # <editor-fold desc="End collapsible section">
# my $timestamp2 = POSIX::strftime("%s", localtime);
# print "\e[0Ksection_end:${timestamp2}:$filename\r\e[0K\n";  # close collapsible section
# # </editor-fold>
