# Test that LoggedSubprocess works (and works reentrantly no less)
import shutil
import unittest
from promise import Promise
import os
import tempfile
# use ksb;
# use Test::More;
#
# use File::Temp qw(tempdir);
# use POSIX;
# use File::Basename;

from ksblib.Module.Module import Module
from ksblib.BuildContext import BuildContext
from ksblib.BuildSystem.BuildSystem import BuildSystem
from ksblib.Util.LoggedSubprocess import Util_LoggedSubprocess

# # <editor-fold desc="Begin collapsible section">
# my $timestamp1 = POSIX::strftime("%s", localtime);
# my $filename = basename(__FILE__);
# my $section_header = "File: $filename (click to toggle collapse)";
# print "\e[0Ksection_start:${timestamp1}:$filename\[collapsed=true]\r\e[0K$section_header\n";  # displayed in collapsible section in gitlab ci job log
# # </editor-fold>


class TestApp(unittest.TestCase):
    def test1(self):
        ctx = BuildContext()
        m = Module(ctx, "test")
        
        self.assertEqual(ctx, "BuildContext setup")
        self.assertTrue(m,"ksb::Module setup")
        self.assertEqual(m.name, "test", "ksb::Module has a name")
        
        tmp = tempfile.mkdtemp()
        ctx.setOption({"log-dir": f"{tmp}/kdesrc-build-test"})
        
        def func(mod):
            print(f"Calculating stuff for {mod}")
        
        cmd = Util.LoggedSubprocess()\
            .module(m)\
            .log_to("test-suite-1")\
            .set_command(['perl', '-E', 'my $x = 2 + 2; say qq($x);' ])\
            .chdir_to(tmp)\
            .announcer(func)
        
        self.assertIsInstance(cmd, Util_LoggedSubprocess, "got the right type of cmd")
        
        output = None
        prog1Exit = None
        prog2Exit = None
        
        def func2(cmd, line):
            nonlocal output
            output = line
            output = output.removesuffix("\n")
        
        cmd.on({"child_output": func2})
        
        def func3(exitcode):
            prog1Exit = exitcode
            
            # Create a second LoggedSubprocess while the first one is still alive, even
            # though it is finished.
            cmd2 = Util_LoggedSubprocess()\
                .module(m)\
                .log_to("test-suite-2")\
                .set_command(['perl', '-E', 'my $x = 4 + 4; say qq(here for stdout); die qq(hello);'])\
                .chdir_to(tmp)
            
            def func4(exit2):
                prog2Exit = exit2
            
            promise2 = cmd2.start().then(func4)
            
            return promise2  # Resolve to another promise that requires resolution
        
        promise = cmd.start().then(func3)
        
        self.assertIsInstance(promise, Promise, "A promise should be a promise!")
        Promise.wait(promise)
        
        self.assertEqual(output, "4", "Interior child command successfully completed")
        self.assertEqual(prog1Exit, 0, "Program 1 exited correctly")
        self.assertNotEqual(prog2Exit, 0, "Program 2 failed")
        
        self.assertTrue(os.path.isdir(f"{tmp}/kdesrc-build-test/latest/test"), "Test module had a 'latest' dir setup")
        self.assertTrue(os.path.islink(f"{tmp}/kdesrc-build-test/latest-by-phase/test/test-suite-1.log"), "Test suite 1 phase log created")
        self.assertTrue(os.path.islink(f"{tmp}/kdesrc-build-test/latest-by-phase/test/test-suite-2.log"), "Test suite 2 phase log created")
        
        os.chdir("/")  # ensure we're out of the test directory
        shutil.rmtree(tmp)

# # <editor-fold desc="End collapsible section">
# my $timestamp2 = POSIX::strftime("%s", localtime);
# print "\e[0Ksection_end:${timestamp2}:$filename\r\e[0K\n";  # close collapsible section
# # </editor-fold>
