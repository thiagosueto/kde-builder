# Test that empty num-cores settings (which could lead to blank -j being passed
# to the build in some old configs) have their -j filtered out.
import os
import unittest
# use ksb;
# use Test::More;
# use POSIX;
# use File::Basename;
#
from ksblib.Module.Module import Module
from ksblib.BuildSystem.BuildSystem import BuildSystem
from ksblib.BuildContext import BuildContext

# # <editor-fold desc="Begin collapsible section">
# my $timestamp1 = POSIX::strftime("%s", localtime);
# my $filename = basename(__FILE__);
# my $section_header = "File: $filename (click to toggle collapse)";
# print "\e[0Ksection_start:${timestamp1}:$filename\[collapsed=true]\r\e[0K$section_header\n";  # displayed in collapsible section in gitlab ci job log
# # </editor-fold>


class BuildSystem_Test(BuildSystem):

    madeArguments = []
    
    # Defang the build command and just record the args passed to it
    def safe_make(self, optsRef):
        BuildSystem_Test.madeArguments = optsRef["make-options"]
        return {"was_successful": 1}


class TestApp(unittest.TestCase):
    def test1(self):
        # Setup a shell build system
        ctx = BuildContext()
        module = Module(ctx, "test")
        buildSystem = BuildSystem_Test(module)
        
        # The -j logic will take off one CPU if you ask for too many so try to ensure
        # test cases don't ask for too many.
        max_cores = os.system("nproc")
        max_cores = max_cores.removesuffix("\n")
        max_cores = int(max_cores // 2)
        if max_cores < 2:
            max_cores = 2
        
        testOption = "make-options"
        
        testMatrix = [
            ["a b -j", ["a", "b"], "Empty -j removed at end"],
            ["-j a b", ["a", "b"], "Empty -j removed at beginning"],
            ["a b", ["a", "b"], "Opts without -j left alone"],
            ["-j", [], "Empty -j with no other opts removed"],
            ["a -j 17 b", ["a", "-j", "17", "b"], "Numeric -j left alone"],
            ["a -j17 b", ["a", "-j17", "b"], "Numeric -j left alone"],
        ]
        
        for item in testMatrix:
            testString, resultRef, testName = item
            module.setOption({testOption: testString})
            buildSystem.buildInternal(testOption)
            self.assertListEqual(BuildSystem_Test.madeArguments, resultRef, testName)
            
            module.setOption({"num-cores": max_cores - 1})
            buildSystem.buildInternal(testOption)
            self.assertListEqual(BuildSystem_Test.madeArguments, ['-j', max_cores - 1, resultRef], f"{testName} with num-cores set")
            module.setOption({"num-cores": ""})
        
        testOption = "ninja-options"
        module.setOption({"make-options": "not used"})
        module.setOption({"cmake-generator": "Kate - Ninja"})
        
        for item in testMatrix:
            testString, resultRef, testName = item
            module.setOption({testOption: testString})
            buildSystem.buildInternal(testOption)
            self.assertListEqual(BuildSystem_Test.madeArguments, resultRef, testName)
        
            module.setOption({"num-cores": max_cores - 1})
            buildSystem.buildInternal(testOption)
            self.assertListEqual(BuildSystem_Test.madeArguments, ['-j', max_cores - 1, resultRef], f"{testName} with num-cores set")
            module.setOption({"num-cores": ""})
        

# # <editor-fold desc="End collapsible section">
# my $timestamp2 = POSIX::strftime("%s", localtime);
# print "\e[0Ksection_end:${timestamp2}:$filename\r\e[0K\n";  # close collapsible section
# # </editor-fold>
