# Verify that --no-include-dependencies is recognized and results
# in right value.

import unittest
# use ksb;
# use Test::More;
# use POSIX;
# use File::Basename;
#
from ksblib.Application import Application

# # <editor-fold desc="Begin collapsible section">
# my $timestamp1 = POSIX::strftime("%s", localtime);
# my $filename = basename(__FILE__);
# my $section_header = "File: $filename (click to toggle collapse)";
# print "\e[0Ksection_start:${timestamp1}:$filename\[collapsed=true]\r\e[0K$section_header\n";  # displayed in collapsible section in gitlab ci job log
# # </editor-fold>


# Redefine ksb::Application::_resolveModuleDependencies to avoid requiring metadata
# module.
class Application_Test(Application):
    def _resolveModuleDependencyGraph(self, modules: list):
        newModule = self.module_factory("setmod2")
        
        graph = {
            "setmod1": {
                "votes": {
                    "setmod2": 1,
                    "setmod3": 1
                },
                "build": 1,
                "module": modules[0]
            },
            "setmod2": {
                "votes": {
                    "setmod3": 1
                },
                "build": bool(self.context.getOption("include-dependencies")),
                "module": newModule
            },
            "setmod3": {
                "votes": {},
                "build": 1,
                "module": modules[1]
            }
        }

        result = {
            "graph": graph
        }
        
        return result


class TestApp(unittest.TestCase):
    def setUp(self):
        self.args = "--pretend --rc-file tests/integration/fixtures/sample-rc/kdesrc-buildrc-with-deps --no-include-dependencies setmod1 setmod3".split(" ")
    
    def test1(self):
        app = Application_Test(self.args)
        moduleList = app.modules
        
        self.assertEqual(len(moduleList), 2, "Right number of modules (include-dependencies)")  # todo fix it
        self.assertEqual(moduleList[0].name, "setmod1", "mod list[0] == setmod1")
        self.assertEqual(moduleList[1].name, "setmod3", "mod list[2] == setmod3")
    
    def test2(self):
        self.args.extend(["--ignore-modules", "setmod2"])
        app = Application_Test(self.args)
        moduleList = app.modules
        
        self.assertEqual(len(moduleList), 2, "Right number of modules (include-dependencies+ignore-modules)")
        self.assertEqual(moduleList[0].name, "setmod1", "mod list[0] == setmod1")
        self.assertEqual(moduleList[1].name, "setmod3", "mod list[1] == setmod3")

# # <editor-fold desc="End collapsible section">
# my $timestamp2 = POSIX::strftime("%s", localtime);
# print "\e[0Ksection_end:${timestamp2}:$filename\r\e[0K\n";  # close collapsible section
# # </editor-fold>
