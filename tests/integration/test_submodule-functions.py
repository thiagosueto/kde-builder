# Test submodule-related features

import unittest
import tempfile
import os
import subprocess
import shutil
# use autodie qw(:io);
# use IPC::Cmd qw(run);
# use POSIX;
# use File::Basename;

# # <editor-fold desc="Begin collapsible section">
# my $timestamp1 = POSIX::strftime("%s", localtime);
# my $filename = basename(__FILE__);
# my $section_header = "File: $filename (click to toggle collapse)";
# print "\e[0Ksection_start:${timestamp1}:$filename\[collapsed=true]\r\e[0K$section_header\n";  # displayed in collapsible section in gitlab ci job log
# # </editor-fold>

from ksblib.Updater.Git import Updater_Git


def run_command(command):
    try:
        output = subprocess.check_output(command, stderr=subprocess.STDOUT, timeout=10, universal_newlines=True)
        return True
    except subprocess.CalledProcessError as e:
        return False
    except subprocess.TimeoutExpired as e:
        return False


class TestApp(unittest.TestCase):
    def test1(self):
        # Create an empty directory for a git module, ensure submodule-related things
        # work without a submodule, then add a submodule and ensure that things remain
        # as expected.
        
        dir = tempfile.mkdtemp()
        os.chdir(dir)
        
        # Setup the later submodule
        os.mkdir("submodule")
        os.chdir("submodule")
        
        result = run_command(["git", "init"])
        self.assertTrue(result, "git init worked")
        
        with open("README.md", 'w') as file:
            file.write("Initial content")
        
        result = run_command(["git config --local user.name kdesrc-build".split(" ")])
        if not result:
            raise SystemExit("Can't setup git username, subsequent tests will fail")
        
        result = run_command(["git config --local user.email kdesrc-build@kde.org".split(" ")])
        if not result:
            raise SystemExit("Can't setup git username, subsequent tests will fail")
        
        result = run_command(["git add README.md".split(" ")])
        self.assertTrue(result, "git add file worked")
        
        result = run_command(["git commit -m FirstCommit".split(" ")])
        self.assertTrue(result, "git commit worked")
        
        # Setup a supermodule
        os.chdir(dir)
        
        os.mkdir("supermodule")
        os.chdir("supermodule")
        
        result = run_command(["git init".split(" ")])
        self.assertTrue(result, "git supermodule init worked")
        
        with open("README.md", 'w') as file:
            file.write("Initial content")
        
        result = run_command(["git config --local user.name kdesrc-build".split(" ")])
        if not result:
            raise SystemExit("Can't setup git username, subsequent tests will fail")
        
        result = run_command(["git config --local user.email kdesrc-build@kde.org".split(" ")])
        if not result:
            raise SystemExit("Can't setup git username, subsequent tests will fail")
        
        result = run_command(["git add README.md".split(" ")])
        self.assertTrue(result, "git supermodule add file worked")
        
        result = run_command(["git commit -m FirstCommit".split(" ")])
        self.assertTrue(result, "git supermodule commit worked")
        
        ### Submodule checks
        
        self.assertFalse(Updater_Git._hasSubmodules(), "No submodules detected when none present")
        
        # git now prevents use of local clones of other git repos on the file system
        # unless specifically enabled, due to security risks from symlinks. See
        # https://github.blog/2022-10-18-git-security-vulnerabilities-announced/#cve-2022-39253
        result = run_command(["git -c protocol.file.allow=always submodule add ../submodule".split(" ")])
        self.assertTrue(result, 'git submodule add worked')
        
        self.assertTrue(Updater_Git._hasSubmodules(), "Submodules detected when they are present")
        
        os.chdir("/")  # Allow auto-cleanup
        shutil.rmtree(dir)
        
# # <editor-fold desc="End collapsible section">
# my $timestamp2 = POSIX::strftime("%s", localtime);
# print "\e[0Ksection_end:${timestamp2}:$filename\r\e[0K\n";  # close collapsible section
# # </editor-fold>
