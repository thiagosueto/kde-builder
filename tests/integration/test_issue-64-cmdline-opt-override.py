# Global options in the rc-file can be overridden on the command line just by
# using their option name in a cmdline argument (as long as the argument isn't
# already allocated, that is).
#
# This ensures that global options overridden in this fashion are applied
# before the rc-file is read.
#
# See issue #64

import unittest
# use Test::More;
# use POSIX;
# use File::Basename;
#
from ksblib.Application import Application
# use ksb::Module;

# # <editor-fold desc="Begin collapsible section">
# my $timestamp1 = POSIX::strftime("%s", localtime);
# my $filename = basename(__FILE__);
# my $section_header = "File: $filename (click to toggle collapse)";
# print "\e[0Ksection_start:${timestamp1}:$filename\[collapsed=true]\r\e[0K$section_header\n";  # displayed in collapsible section in gitlab ci job log
# # </editor-fold>


class TestApp(unittest.TestCase):
    def setUp(self):
        self.args = []
    
    def test1(self):
        # The issue used num-cores as an example, but should work just as well
        # with make-options
        self.args = "--pretend --rc-file tests/integration/fixtures/sample-rc/kdesrc-buildrc".split(" ")
        
        app = Application(self.args)
        moduleList = app.modules
        
        self.assertEqual(app.context.getOption("num-cores"), 8, "No cmdline option leaves num-cores value alone")
        
        self.assertEqual(len(moduleList), 4, "Right number of modules")
        self.assertEqual(moduleList[0].name, "setmod1", "mod list[0] == setmod1")
        self.assertEqual(moduleList[0].getOption("make-options"), "-j4", "make-options base value proper pre-override")
        
        self.assertEqual(moduleList[3].name, "module2", "mod list[3] == module2")
        self.assertEqual(moduleList[3].getOption("make-options"), "-j 8", "module-override make-options proper pre-override")
    
    def test2(self):
        # We can't seem to assign -j3 as Getopt::Long will try to understand the option
        # and fail
        self.args.extend(["--make-options", "j3"])
        
        app = Application(self.args)
        moduleList = app.modules
        
        self.assertEqual(app.context.getOption("num-cores"), 8, "No cmdline option leaves num-cores value alone")
        
        self.assertEqual(len(moduleList), 4, "Right number of modules")
        self.assertEqual(moduleList[0].name, "setmod1", "mod list[0] == setmod1")
        self.assertEqual(moduleList[0].getOption("make-options"), "j3", "make-options base value proper post-override")
        
        # Policy discussion: Should command line options override *all* instances
        # of an option in kdesrc-buildrc? Historically the answer has deliberately
        # been yes, so that's the behavior we enforce.
        self.assertEqual(moduleList[3].name, "module2", "mod list[3] == module2")
        self.assertEqual(moduleList[3].getOption("make-options"), "j3", "module-override make-options proper post-override")

    def test3(self):
        # Remove last two args and add another test of indirect option value setting
        self.args.pop()
        self.args.pop()
        self.args.append("--num-cores=5")  # 4 is default, 8 is in rc-file, use something different
        
        app = Application(self.args)
        moduleList = app.modules
        
        self.assertEqual(app.context.getOption("num-cores"), 5, "Updated cmdline option changes global value")
        
        self.assertEqual(len(moduleList), 4, "Right number of modules")
        self.assertEqual(moduleList[0].name, "setmod1", "mod list[0] == setmod1")
        self.assertEqual(moduleList[0].getOption("make-options"), "-j4", "make-options base value proper post-override (indirect value)")
        
        self.assertEqual(moduleList[3].name, "module2", "mod list[3] == module2")
        self.assertEqual(moduleList[3].getOption("make-options"), "-j 5", "module-override make-options proper post-override (indirect value)")

# # <editor-fold desc="End collapsible section">
# my $timestamp2 = POSIX::strftime("%s", localtime);
# print "\e[0Ksection_end:${timestamp2}:$filename\r\e[0K\n";  # close collapsible section
# # </editor-fold>
