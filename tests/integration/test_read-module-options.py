# Test basic option reading from rc-files

import unittest
# use ksb::Util (); # load early so we can override
#
# use Mojo::Util qw(monkey_patch);
# use Mojo::Promise;
#
# use Test::More;
# use Carp qw(confess);
# use POSIX;
# use File::Basename;

# # <editor-fold desc="Begin collapsible section">
# my $timestamp1 = POSIX::strftime("%s", localtime);
# my $filename = basename(__FILE__);
# my $section_header = "File: $filename (click to toggle collapse)";
# print "\e[0Ksection_start:${timestamp1}:$filename\[collapsed=true]\r\e[0K$section_header\n";  # displayed in collapsible section in gitlab ci job log
# # </editor-fold>

# # Override ksb::Util::log_command for final test to see if it is called with
# # 'cmake'
#
# # Very important that this happens in a BEGIN block so that it happens before
# # ksb::Application is loaded, so that ksb::Util's log_command method can be
# # overridden before it is copied to dependents packages' symbol tables
# BEGIN {
#     monkey_patch('ksb::Util',
#         run_logged_p => sub ($module, $filename, $dir, $argRef) {
#             confess "No arg to module" unless $argRef;
#             my @command = @{$argRef};
#             if (grep { $_ eq 'cmake' } @command) {
#                 @CMD = @command;
#             }
#             return Mojo::Promise->resolve(0);
#         });
# }


# Now we can load ksb::Application, which will load a bunch more modules all
# using log_command and run_logged_p from ksb::Util
from ksblib.Application import Application
from ksblib.Updater.Git import Updater_Git


class TestApp(unittest.TestCase):
    def test1(self):
        CMD = []
        app = Application("--pretend --rc-file tests/integration/fixtures/sample-rc/kdesrc-buildrc".split(" "))
        moduleList = app.modules
        
        self.assertEqual(len(moduleList), 4, "Right number of modules")
        
        # module2 is last in rc-file so should sort last
        self.assertEqual(moduleList[3].name, "module2", "Right module name")
        
        scm = moduleList[3].scm()
        self.assertIsInstance(scm, Updater_Git)
        
        branch, type = scm._determinePreferredCheckoutSource()
        
        self.assertEqual(branch, "refs/tags/fake-tag5", "Right tag name")
        self.assertEqual(type, "tag", "Result came back as a tag")
        
        # setmod2 is second module in set of 3 at start, should be second overall
        self.assertEqual(moduleList[1].name, "setmod2", "Right module name from module-set")
        branch, type = moduleList[1].scm()._determinePreferredCheckoutSource()
        
        self.assertEqual(branch, "refs/tags/tag-setmod2", "Right tag name (options block)")
        self.assertEqual(type, "tag", "options block came back as tag")
        
        # Test some of the option parsing indirectly by seeing how the value is input
        # into build system.
        
        # Override auto-detection since no source is downloaded
        moduleList[1].setOption("override-build-system", "kde")
        
        # Should do nothing in --pretend
        self.assertTrue(moduleList[1].setupBuildSystem(), 'setup fake build system')
        
        self.assertTrue(CMD, "run_logged_p cmake was called")
        self.assertEqual(len(CMD), 12)
        
        self.assertEqual(CMD[0], "cmake", "CMake command should start with cmake")
        self.assertEqual(CMD[1], "-B",    "Passed build dir to cmake")
        self.assertEqual(CMD[2], ".",     "Passed cur dir as build dir to cmake")
        self.assertEqual(CMD[3], "-S",    "Pass source dir to cmake")
        self.assertEqual(CMD[4], "/tmp/setmod2", "CMake command should specify source directory after -S")
        self.assertEqual(CMD[5], "-G", "CMake generator should be specified explicitly")
        self.assertEqual(CMD[6], "Unix Makefiles", "Expect the default CMake generator to be used")
        self.assertEqual(CMD[7], "-DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=ON", "Per default we generate compile_commands.json")
        self.assertEqual(CMD[8], "-DCMAKE_BUILD_TYPE=a b", "CMake options can be quoted")
        self.assertEqual(CMD[9], "bar=c", "CMake option quoting does not eat all options")
        self.assertEqual(CMD[10], "baz", "Plain CMake options are preserved correctly")
        self.assertEqual(CMD[11], "-DCMAKE_INSTALL_PREFIX=$ENV{HOME}/kde/usr", "Prefix is passed to cmake")
        
        # See https://phabricator.kde.org/D18165
        self.assertEqual(moduleList[0].getOption("cxxflags"), "", "empty cxxflags renders with no whitespace in module")

# # <editor-fold desc="End collapsible section">
# my $timestamp2 = POSIX::strftime("%s", localtime);
# print "\e[0Ksection_end:${timestamp2}:$filename\r\e[0K\n";  # close collapsible section
# # </editor-fold>
