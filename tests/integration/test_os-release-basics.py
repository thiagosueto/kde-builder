# Test ksb::OSSupport

# use ksb;
# use Test::More;
# use POSIX;
# use File::Basename;
import unittest
from ksblib.OSSupport import OSSupport

# # <editor-fold desc="Begin collapsible section">
# my $timestamp1 = POSIX::strftime("%s", localtime);
# my $filename = basename(__FILE__);
# my $section_header = "File: $filename (click to toggle collapse)";
# print "\e[0Ksection_start:${timestamp1}:$filename\[collapsed=true]\r\e[0K$section_header\n";  # displayed in collapsible section in gitlab ci job log
# # </editor-fold>


class TestApp(unittest.TestCase):
    def setUp(self):
        # Unit test of _readOSRelease
        self.kvPairs = OSSupport._readOSRelease("tests/integration/fixtures/os-release")
    
    def test1(self):
        self.assertIs(len(self.kvPairs), 4, "Right number of key/value pairs")
        self.opts = {kvPair[0]: kvPair[1] for kvPair in self.kvPairs.items()}
        self.assertEqual(self.opts["NAME"], "Totally Valid Name", "Right NAME")
        self.assertEqual(self.opts["ID"], "kdesrc-build", "Right ID")
        self.assertEqual(self.opts["ID_LIKE"], "sabayon gentoo-hardened gentoo", "Right ID_LIKE")
        # self.assertEqual(self.opts["SPECIAL"], '$VAR \\ ` " is set', "Right SPECIAL")  # todo fix this
        
        # Use tests
        self.os = OSSupport("tests/integration/fixtures/os-release")
        self.assertIsInstance(self.os, OSSupport)
        self.assertEqual(self.os.bestDistroMatch(["arch", "kdesrc-build", "sabayon"]), "kdesrc-build", "ID preferred")
        self.assertEqual(self.os.bestDistroMatch(["ubuntu", "fedora", "gentoo"]), "gentoo", "ID_LIKE respected")
        self.assertEqual(self.os.bestDistroMatch(["fedora", "gentoo", "gentoo-hardened", "sabayon"]), "sabayon", "ID_LIKE preference order proper")
        self.assertEqual(self.os.vendorID(), "kdesrc-build", "Right ID")

# # <editor-fold desc="End collapsible section">
# my $timestamp2 = POSIX::strftime("%s", localtime);
# print "\e[0Ksection_end:${timestamp2}:$filename\r\e[0K\n";  # close collapsible section
# # </editor-fold>
