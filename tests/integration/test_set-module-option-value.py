# Test use of --set-module-option-value

# use ksb;
# use Test::More;
# use POSIX;
# use File::Basename;
import unittest
from ksblib.Application import Application

# # <editor-fold desc="Begin collapsible section">
# my $timestamp1 = POSIX::strftime("%s", localtime);
# my $filename = basename(__FILE__);
# my $section_header = "File: $filename (click to toggle collapse)";
# print "\e[0Ksection_start:${timestamp1}:$filename\[collapsed=true]\r\e[0K$section_header\n";  # displayed in collapsible section in gitlab ci job log
# # </editor-fold>


class TestApp(unittest.TestCase):
    def setUp(self):
        self.app = Application("--pretend --rc-file tests/integration/fixtures/sample-rc/kdesrc-buildrc --set-module-option-value module2,tag,fake-tag10 --set-module-option-value setmod2,tag,tag-setmod10".split(" "))
        self.moduleList = self.app.modules
    
    def test1(self):
        self.assertEqual(len(self.moduleList), 4, "Right number of modules")
        
        module = [m for m in self.moduleList if f"{m}" == "module2"][0]
        scm = module.scm()
        branch, type = scm._determinePreferredCheckoutSource()
        
        self.assertEqual(branch, "refs/tags/fake-tag10", "Right tag name")  # todo fix it
        self.assertEqual(type, "tag", "Result came back as a tag")
        
        module = [m for m in self.moduleList if f"{m}" == "setmod2"][0]
        branch, type = module.scm()._determinePreferredCheckoutSource()
        
        self.assertEqual(branch, "refs/tags/tag-setmod10", "Right tag name (options block from cmdline)")
        self.assertEqual(type, "tag", "cmdline options block came back as tag")
        
        self.assertFalse(module.isKDEProject(), "setmod2 is *not* a \"KDE\" project")
        self.assertEqual(module.fullProjectPath(), "setmod2", 'fullProjectPath on non-KDE modules returns name')

# # <editor-fold desc="End collapsible section">
# my $timestamp2 = POSIX::strftime("%s", localtime);
# print "\e[0Ksection_end:${timestamp2}:$filename\r\e[0K\n";  # close collapsible section
# # </editor-fold>
