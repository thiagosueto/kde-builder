# Verify that a user-set CMAKE_PREFIX_PATH is not removed, even if we supply
# "magic" of our own
# See bug 395627 -- https://bugs.kde.org/show_bug.cgi?id=395627

# use ksb;
#
# use Test::More;
# use POSIX;
# use File::Basename;

# # <editor-fold desc="Begin collapsible section">
# my $timestamp1 = POSIX::strftime("%s", localtime);
# my $filename = basename(__FILE__);
# my $section_header = "File: $filename (click to toggle collapse)";
# print "\e[0Ksection_start:${timestamp1}:$filename\[collapsed=true]\r\e[0K$section_header\n";  # displayed in collapsible section in gitlab ci job log
# # </editor-fold>


import unittest
from ksblib.Util import Util
from ksblib.Application import Application
from ksblib.BuildSystem.KDECMake import BuildSystem_KDECMake
from promise import Promise


class TestApp(unittest.TestCase):
    def setUp(self):
        self.args = "--pretend --rc-file tests/integration/fixtures/bug-395627/kdesrc-buildrc".split(" ")
        self.savedCommand = []
        self.log_called = 0
        
        # Redefine log_command to capture whether it was properly called. This is all
        # very order-dependent, we need to load ksb::Util before kdesrc-build itself
        # does to install the new subroutine before it's copied over into the other
        # package symbol tables.
        
        def redefined_run_logged_p(module, filename, dir, argRef):
            self.log_called = 1
            self.savedCommand = argRef
            return Promise.resolve(0)  # success
        
        Util.run_logged_p = redefined_run_logged_p
        
    def test1(self):
        app = Application(self.args)
        moduleList = app.modules
        
        self.assertEqual(len(moduleList), 6, "Right number of modules")
        self.assertIsInstance(moduleList[0].buildSystem(), BuildSystem_KDECMake)
        
        # This requires log_command to be overridden above
        result = moduleList[0].setupBuildSystem()
        self.assertEqual(self.log_called, 1, "Overridden log_command was called")
        self.assertTrue(result, "Setup build system for auto-set prefix path")
        
        # We should expect an auto-set -DCMAKE_PREFIX_PATH passed to cmake somewhere
        prefix = next((x for x in self.savedCommand if "-DCMAKE_PREFIX_PATH" in x), None)
        self.assertEqual(prefix, "-DCMAKE_PREFIX_PATH=/tmp/qt5", "Prefix path set to custom Qt prefix")
        
        result = moduleList[2].setupBuildSystem()
        self.assertTrue(result, "Setup build system for manual-set prefix path")
        
        prefixes = [el for el in self.savedCommand if "-DCMAKE_PREFIX_PATH" in el]
        self.assertEqual(len(prefixes), 1, "Only one set prefix path in manual mode")
        if prefixes:
            self.assertEqual(prefixes[0], "-DCMAKE_PREFIX_PATH=FOO", "Manual-set prefix path is as set by user")
        
        result = moduleList[4].setupBuildSystem()
        self.assertTrue(result, "Setup build system for manual-set prefix path")
        
        prefixes = [el for el in self.savedCommand if "-DCMAKE_PREFIX_PATH" in el]
        self.assertEqual(len(prefixes), 1, "Only one set prefix path in manual mode")
        if prefixes:
            self.assertEqual(prefixes[0], "-DCMAKE_PREFIX_PATH:PATH=BAR", "Manual-set prefix path is as set by user")
        
# # <editor-fold desc="End collapsible section">
# my $timestamp2 = POSIX::strftime("%s", localtime);
# print "\e[0Ksection_end:${timestamp2}:$filename\r\e[0K\n";  # close collapsible section
# # </editor-fold>
