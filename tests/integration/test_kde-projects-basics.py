# Verify that test kde-project data still results in workable build.

import unittest
# use ksb;
# use Test::More;
# use POSIX;
# use File::Basename;
#
from ksblib.Application import Application
# use ksb::Module;

# # <editor-fold desc="Begin collapsible section">
# my $timestamp1 = POSIX::strftime("%s", localtime);
# my $filename = basename(__FILE__);
# my $section_header = "File: $filename (click to toggle collapse)";
# print "\e[0Ksection_start:${timestamp1}:$filename\[collapsed=true]\r\e[0K$section_header\n";  # displayed in collapsible section in gitlab ci job log
# # </editor-fold>


class TestApp(unittest.TestCase):
    def setUp(self):
        # The file has a module-set that only refers to juk but should expand to
        # kcalc juk in that order
        self.args = "--pretend --rc-file tests/integration/fixtures/kde-projects/kdesrc-buildrc-with-deps".split(" ")
    
    def test1(self):
        app = Application(self.args)
        moduleList = app.modules
        
        self.assertEqual(len(moduleList), 3, "Right number of modules (include-dependencies)")
        self.assertEqual(moduleList[0].name, "kcalc", "Right order: kcalc before juk (test dep data)")
        self.assertEqual(moduleList[1].name, "juk", "Right order: juk after kcalc (test dep data)")
        self.assertEqual(moduleList[2].name, "kdesrc-build", "Right order: dolphin after juk (implicit order)")
        self.assertEqual(moduleList[0].getOption("tag"), "tag-setmod2", "options block works for indirect reference to kde-projects module")  # todo fix it
        self.assertEqual(moduleList[0].getOption("cmake-generator"), "Ninja", "Global opts seen even with other options")
        self.assertEqual(moduleList[1].getOption("cmake-generator"), "Make", "options block works for kde-projects module-set")
        self.assertEqual(moduleList[1].getOption("cmake-options"), "-DSET_FOO:BOOL=ON", "module options block can override set options block")
        self.assertEqual(moduleList[2].getOption("cmake-generator"), 'Make', "options block works for kde-projects module-set after options")
        self.assertEqual(moduleList[2].getOption("cmake-options"), "-DSET_FOO:BOOL=ON", "module-set after options can override options block")

# # <editor-fold desc="End collapsible section">
# my $timestamp2 = POSIX::strftime("%s", localtime);
# print "\e[0Ksection_end:${timestamp2}:$filename\r\e[0K\n";  # close collapsible section
# # </editor-fold>
