# Test tag names based on time

import unittest
from ksblib.Application import Application
from ksblib.Updater.Git import Updater_Git

# # <editor-fold desc="Begin collapsible section">
# my $timestamp1 = POSIX::strftime("%s", localtime);
# my $filename = basename(__FILE__);
# my $section_header = "File: $filename (click to toggle collapse)";
# print "\e[0Ksection_start:${timestamp1}:$filename\[collapsed=true]\r\e[0K$section_header\n";  # displayed in collapsible section in gitlab ci job log
# # </editor-fold>


class TestApp(unittest.TestCase):
    def setUp(self):
        self.app = Application(["--pretend", "--rc-file", "tests/integration/fixtures/branch-time-based/kdesrc-buildrc"])
        self.moduleList = self.app.modules
    
    def test_app(self):
        
        self.assertIs(len(self.moduleList), 3, "Right number of modules")
        
        for mod in self.moduleList:
            scm = mod.scm()
            self.assertIsInstance(scm, Updater_Git)
            
            branch, sourcetype = scm._determinePreferredCheckoutSource()
            self.assertEqual(branch, "master@{3 weeks ago}", "Right tag name")
            self.assertEqual(sourcetype, "tag", "Result came back as a tag with detached HEAD")

# # <editor-fold desc="End collapsible section">
# my $timestamp2 = POSIX::strftime("%s", localtime);
# print "\e[0Ksection_end:${timestamp2}:$filename\r\e[0K\n";  # close collapsible section
# # </editor-fold>
