# KDE Builder

This script streamlines the process of setting up and maintaining a development
environment for KDE software.

It does this by automating the process of downloading source code from the
KDE source code repositories, building that source code, and installing it
to your local system.

### The goal of the project

**KDE Builder** is a drop-in replacement for the **Kdesrc Build** project. It is the exact reimplementation
of the predecessor script, but in python - a much more acknowledged language. The original project is in perl,
and this is a significant barrier for new contributions.

After switching to this project, those much wanted features (see the bugtracker) can be implemented with ease.

## Testing welcome (January 2024)

This project is not yet "officially" released. Current state is "should work", but
there are some things that needs to be finished:

- Unit and Integration tests needs to be made working
- Todo comments in the project needs to be handled
- Final testing of using this project

You can help with the last point. Follow the installation procedure, then try
to use `kde-builder` the same as you normally run the `kesrc-build`.

If you found something not working in `kde-builder` that works in `kdesrc-build`,
please report it. You can write in the matrix chat (see [here](https://community.kde.org/Get_Involved/development#Where_to_find_the_development_team))
or creating an issue in the [project repo](https://invent.kde.org/ashark/kde-builder).

### Updating your clone

Currently, the project history is periodically overwritten by force-pushing to master. This way the project will have
clean history at the moment of "official launching" (presumably at the 28 February 2024, the Mega 
Release date).

So, if you have cloned the repo, and then want to pull new updates, you can do this like this:

```bash
$ git pull --rebase
```

## Installation

This project it targeting python version 3.12.

Install python 3.12. For example in Arch Linux you will run (assuming you use yay):

```bash
$ yay -S python312
```

Install pipenv. In Arch Linux the command will be:

```bash
$ sudo pacman -S python-pipenv 
```

Open the terminal in the directory where you would store the project and clone it:

```bash
$ cd ~/Development
$ git clone git@invent.kde.org:ashark/kde-builder.git
$ cd kde-builder
```

Create a virtual environment with the required packages:

```bash
$ pipenv install
```

Now you can run the script like this: 

```bash
$ pipenv run python kde-builder --pretend kcalc
```

For convenience, you can create a wrapper script.
Create a file `~/bin/kde-builder` (assuming the `~/bin` is
in your PATH), make this file executable. Add the following content to it:

```bash
#!/bin/bash

cd ~/Development/kde-builder
pipenv run python kde-builder $@
```

Now you can invoke the script like this:

```bash
$ kde-builder --pretend kcalc
```

## Documentation

See the wiki page [Get_Involved/development](https://community.kde.org/Get_Involved/development).
It covers usage of `kdesrc-build` script - a perl predecessor of `kde-builder`.

For more details, consult the project documentation. The most important pages are:

- [List of supported configuration options](https://docs.kde.org/trunk5/en/kdesrc-build/kdesrc-build/conf-options-table.html)
- [Supported command line parameters](https://docs.kde.org/trunk5/en/kdesrc-build/kdesrc-build/supported-cmdline-params.html)
